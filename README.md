# React-MoviDB (React SSR + IndexedDB)

This Project was combination of React SSR + IndexedDB

#DEMO
[http://18.222.220.62:3060/](http://18.222.220.62:3060/)

## Table of Contents
1. [Requirements](#Minimum Requirements)
2. [Getting Started](#Getting Started)
2. [Service Handling](#Service Handling)

## Minimum Requirements
* node `8.11.1 (LTS)`
* npm `5.6.0`

## Configuration (Local Machine)
- Copy Environment configuration file from .env.example to .env
```bash
$ cp .env.example .env
```
- Modify it based on your configuration preference

## Configuration (Server Machine)
There are two method on how to set the environment configuration :
- Using .env File
  - Copy Environment configuration file from .env.example to .env
    ```bash
    $ cp .env.example .env
    ```
  
  - Modify it based on server configuration preference
- Using Machine Environment
  - Edit ~/.bash_profile or create a bash script that run the server
  - Put all the required environment configuration there
  - ``` 
    # NodeJS Environment
    NODE_ENV=development
    
    # Cluster worker count
    # Remove Comment to limit workers
    RECLUSTER_WORKERS=1
    
    # PID File Output
    #PIDFILE=/var/run/interview-frontend-web.pid
    
    HOST=localhost
    PORT=3000
    SECRET=197eadef69e4957631a5c3cf97961b33
    
    # Host Configuration example: https://interview-frontend-web
    APPHOST=http://localhost:3000
    APPDOMAIN=
    
    # API SEARCH SOURCES
    OMDB_API=http://www.omdbapi.com/
    OMDB_API_KEY=7f29540e
    
    # Log configuration
    #LOGDIR=/var/logs/
    #LOG_SIZE=500m
    #LOG_KEEP=5

    

## Getting Started

After confirming that your development environment meets the specified [requirements](#Minimum Requirements),
you can start the site by running these commands:

```bash
$ cp .env.example .env          # Environment config file is required
$ npm install                   # Install project dependencies
$ npm start                     # Compile and launch
```

While developing, you will probably rely mostly on `npm start`; however, there are additional scripts at your disposal:

|`npm run <script>`|Description|
|------------------|-----------|
|`start` |Serves your app at `localhost:3000`. HMR will be enabled and `client.css` file will not be generated.|
|`build`|Compiles the application to disk (`~/build` by default).|
|`serve` |Serves your builded app at `localhost:3000`. Using `./bin/cluster`, to simulate production behaviour.|
|`test`|Runs all tests in sequence|
|`lint:js`|Run javascript linter.|
  
***Important note:***

Before you commit, make sure to always run:

```bash
$ npm test
```

and have all the tests pass.

## Service Handling

**CPU USAGE**  
By default app will use all of the CPU core to spawn it's worker,
this can be changed by modifying `RECLUSTER_WORKERS=1` to any CPU count you require

**GRACEFUL RESTART**  
To make sure that all process were not used before killing the process
and gracefully kill the child process use :  
``kill -s SIGUSR2 `cat /var/project-path/project-name.pid` ``  

to change PID file output location you could modify the env `PIDFILE=/var/project-name.pid`


**GRACEFUL KILL**  
``kill -s SIGTERM `cat /var/project-path/project-name.pid` ``

## Branches
There are following branches used in project:
* `master` that is what is running on production server
* `devel` is always on top of `master` (fast-forward) and that is branch you should fork when you start working on a new feature
That is also the branch you should rebase onto daily while you are working on your feature.
* `feature/*` each feature may have it's own branch and that branch may be deployed somewhere for testing as well.

