import {
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import { getMovieSearch } from '../../../models';
import JSONType from '../../types/JSONType';

const movieSearch = {
  type: JSONType,
  args: {
    search: {type: GraphQLString},
    year: {type: GraphQLString},
    page: {type: GraphQLInt}
  },
  resolve: (root, args) => {
    const {page, year, search} = args || {};
    const {client} = root || {};

    return getMovieSearch(client)({
      s: search,
      ...(page ? { page } : {}),
      ...((year && year !== '') ? { y: year } : {})
    });

  }
}

export default movieSearch;