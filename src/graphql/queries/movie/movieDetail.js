import { GraphQLString } from 'graphql';
import { getMovieDetail } from '../../../models';
import JSONType from '../../types/JSONType';

const movieDetail = {
  type: JSONType,
  args: {
    id: { type: GraphQLString }
  },
  resolve: (root, args) => {
    const { id } = args || {};
    const {client} = root || {};

    return getMovieDetail(client)({
      i: id
    });
  }
}

export default movieDetail;