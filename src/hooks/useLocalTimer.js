import React, { useEffect, useState } from 'react';
import moment from 'moment';

const useLocalTimer = (interval = 1000) => {
  const [ time, setTime ] = useState(moment());

  useEffect(() => {
    const timeInterval = setInterval(() => {
      setTime(moment());
    }, interval);

    return () => {
      clearInterval(timeInterval);
    };
  }, []);

  return time;
};

export default useLocalTimer;
