import {canUseDOM} from "fbjs/lib/ExecutionEnvironment";

class IndexedDB {
  constructor(name, version = 1) {
    this.name = name;
    this.version = version;

    if (!canUseDOM) {
      return {
        load: () => {},
        get: () => {},
        add: () => {},
        remove: () => {}
      };
    }

    const self = this;

    this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    this.openRequest = this.indexedDB.open(this.name, this.version);
    this.openRequest.onerror = function(event) {
      console.log(event)
    };
    this.openRequest.onsuccess = function(event) {
      self.db = self.openRequest.result;
    };
    this.openRequest.onupgradeneeded = function(event) {
      const db = event.target.result;

      db.onerror = console.log;
      db.createObjectStore(self.name, { keyPath: "id" });
    };
  }

  load () {
    const self = this;

    if (!self.db) {
      return;
    }

    return new Promise((resolve, reject) => {
      let results = [];
      const objectStore = self.db.transaction(self.name).objectStore(self.name);
      const getRequest = objectStore.getAll();

      getRequest.onsuccess = function(event) {
        if (getRequest.result) {
          results = getRequest.result;
        }

        resolve(results);
      }
    })
  }

  get (value) {
    const self = this;

    if (!self.db) {
      return;
    }

    return new Promise((resolve, reject) => {
      const transaction = self.db.transaction([self.name], 'readonly');
      const objectStore = transaction.objectStore(self.name);

      transaction.onerror = reject;

      const getRequest = objectStore.get(value);

      getRequest.onerror = reject;

      getRequest.onsuccess = function() {
          const cursor = getRequest.result;

        if (cursor) {
          resolve(cursor);
        }

        resolve(null);
      }

      getRequest.onerror = reject;
    })
  }

  add (data, onSuccess) {
    if (!this.db) {
      return;
    }

    const transaction = this.db.transaction([this.name], "readwrite");

    transaction.oncomplete = console.log;
    transaction.onerror = console.error;

    const objectStore = transaction.objectStore(this.name);
    const objectStoreRequest = objectStore.add(data);

    objectStoreRequest.onsuccess = onSuccess;
  }

  remove(data, onSuccess) {
    if (!this.db) {
      return;
    }

    const transaction = this.db.transaction([this.name], "readwrite");

    transaction.objectStore(this.name).delete(data);
    transaction.oncomplete = onSuccess;
  }
}

export default IndexedDB;