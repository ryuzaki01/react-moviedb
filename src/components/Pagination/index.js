import React from 'react';
import { Link } from '@tiket-com/react-ui';
import { useHistory, useLocation } from 'react-router-dom';
import { number, func, bool } from 'prop-types';

import { parseQuery, stringifyQuery, range } from '../../core/utils';

import './styles.scss';

const paginate = (curPage, pageCount) => {
  const adjacent = 3;
  const currentPage = Number(curPage);

  const rangeWithDots = [];

  rangeWithDots.unshift(currentPage);

  range(adjacent).forEach(function (val) {
    const page = (currentPage - (val + 1));

    if (page > 0) {
      rangeWithDots.unshift(page);
    }
  });

  range(adjacent).forEach(function (val) {
    const page = (currentPage + val + 1);

    if (page <= pageCount) {
      rangeWithDots.push(page);
    }
  });

  return rangeWithDots;
};

const Pagination = (props) => {
  const { lastPage, onSet = (p) => {} } = props;
  const history = useHistory();
  const { pathname, search } = useLocation();
  const query = parseQuery(search);
  const { p } = query || {};
  const currentPage = parseInt(p || '1', 10);

  const handlePagination = (page) =>{
    onSet(page);

    history.push({
      pathname,
      search: `?${stringifyQuery({
        ...query,
        p: page
      })}`
    });
  };

  const paginated = paginate(currentPage, lastPage);

  if (paginated.length <= 1) {
    return (
      <div className="pagination" />
    );
  }

  return (
    <div className="pagination">
      <ul>
        {currentPage > 1 && (
          <li className="previous" onClick={() => handlePagination(currentPage - 1)}>
            ◀
          </li>
        )}
        {(paginated || []).map((p, i) => {
          const isDisabled = currentPage === p || p === "...";

          return (
            <li
              key={`paginate-${i}`}
              className={isDisabled ? 'active' : ''}
              onClick={() => handlePagination(p)}
            >{p}</li>
          )
        })}
        {currentPage < lastPage && (
          <li className="next"  onClick={() => handlePagination(currentPage + 1)}>
            ▶
          </li>
        )}
      </ul>
    </div>
  )
};

Pagination.propTypes = {
  currentPage: number,
  lastPage: number,
  onSet: func,
  log: bool
};

export default Pagination;