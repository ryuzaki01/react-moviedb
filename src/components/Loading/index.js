import React from 'react';
import { bool } from 'prop-types';

import loadingImg from './assets/loading.svg';
import './styles.scss';

const Loading = (props) => {
  const { full } = props;

  return (
    <div className={`loading-container ${full ? 'full' : ''}`}>
      <img src={loadingImg}/>
    </div>
  )
};

Loading.propTypes = {
  full: bool
}

export default Loading;