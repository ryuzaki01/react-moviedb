import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '@tiket-com/react-ui';

import './style.scss';

const Header = () => {
  return (
    <div className="header">
      <Link to="/">
        <h1>React MovieDB</h1>
      </Link>
      <Link to="/favorite">
        <Icon icon="star-full" className="favorite-icon"/>
      </Link>
    </div >
  );
};

export default Header;
