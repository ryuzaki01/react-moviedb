import React from 'react';

import './styles.scss';

const Button = (props) => {
  const { className, children, ...restProps } = props;

  return (
    <button className={`btn ${className}`}>{children}</button>
  );
};

export default Button;