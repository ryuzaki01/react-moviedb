import React from 'react';
import { object } from 'prop-types';
import { Link } from 'react-router-dom';

import Card from '../Card';
import NoImage from '../../assets/no-image.png';

import './styles.scss';

const Index = (props) => {
  const { data } = props;
  const { imdbID, Type, Title, Poster, Year } = data || {};

  return (
    <Link to={`/detail/${imdbID}`}>
      <Card className="result-item">
        <div className="thumb">
          <img src={Poster === 'N/A' ? NoImage : Poster}/>
        </div>
        <div className="desc">
          <h3>{Title}</h3>
          <p>{`Type: ${i18n(`type.${Type}`)}`}</p>
          <p>{`Year: ${Year}`}</p>
        </div>
      </Card>
    </Link>
  )
}

Index.propTypes = {
  data: object
}

export default Index;
