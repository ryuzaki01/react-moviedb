import React, { useEffect } from 'react';
import { object } from 'prop-types';
import { renderRoutes } from 'react-router-config';
import { useLocation } from "react-router-dom";

import Header from '../Header';
import useData from '../../hooks/useData';
import { parseQuery } from '../../core/utils';

import './style.scss';

const Layout = ({ route }) => {
  const [data, setData ] = useData();
  const { search, pathname } = useLocation();

  useEffect(() => {
    setData({
      ...data,
      query: parseQuery(search),
      pathname
    });
  }, [search, pathname]);

  return (
    <div className="wrapper">
      <Header />
      <div className="content">
        {renderRoutes(route.routes)}
      </div>
    </div>
  );
};

Layout.propTypes = {
  route: object,
};

export default Layout;
