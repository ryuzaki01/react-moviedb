export const fontsCSS = `@font-face {
  font-family: system;
  font-style: normal;
  font-display: swap;
  font-weight: normal;
  src: local("Arial"), local(".SFNSText-Light"), local(".HelveticaNeueDeskInterface-Light"), local(".LucidaGrandeUI"), local("Ubuntu Light"), local("Segoe UI Light"), local("Roboto-Light"), local("DroidSans"), local("Tahoma");
}`;

export const loader = `.loading-container {
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    vertical-align: middle;
  }

  &.full {
    height: 100vh;
  }
}`;