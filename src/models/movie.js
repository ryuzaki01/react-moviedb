import config from 'config';

export const getMovieSearch = (client) => (data) => {
  return client(config.OMDB_API, {
    method: 'GET',
    data: {
      ...data,
      apiKey: config.OMDB_API_KEY
    }
  });
}

export const getMovieDetail = (client) => (data) => {
  return client(config.OMDB_API, {
    method: 'GET',
    data: {
      ...data,
      plot: 'full',
      apiKey: config.OMDB_API_KEY
    }
  });
}