import config from 'config';

export const getHackerNewsSearch = (client) => (data) => {
  return client(`${config.API_HACKER_NEWS}/search`, {
    method: 'GET',
    data,
    redirect: 'follow'
  });
}

export const getHackerNewsAuthor = (client) => (data) => {
  return client(`${config.API_HACKER_NEWS}/users/${data}`, {
    method: 'GET'
  });
}