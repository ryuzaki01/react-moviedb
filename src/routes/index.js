import React from 'react';
import loadable from '@loadable/component';
import Layout from '../components/Layout';

const Home = loadable(() => import(/* webpackChunkName: "Home" */ './Home'));
const Detail = loadable(() => import(/* webpackChunkName: "Detail" */ './Detail'));
const Favorite = loadable(() => import(/* webpackChunkName: "Favorite" */ './Favorite'));
const NotFound = loadable(() => import(/* webpackChunkName: "NotFound" */ './NotFound'));

export default [
  {
    component: Layout,
    routes: [
      {
        path: "/",
        exact: true,
        component: Home
      },
      {
        path: "/detail/:id",
        component: Detail
      },
      {
        path: "/favorite",
        component: Favorite
      },
      {
        path: "*",
        component: NotFound
      }
    ]
  }
];
