import React, { useEffect, useState, useCallback } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { useHistory, useLocation } from 'react-router-dom';

import ResultItem from "../../components/ResultItem";
import Pagination from '../../components/Pagination';
import Loading from '../../components/Loading';
import Button from '../../components/Button';

import { range, parseQuery, stringifyQuery } from "../../core/utils";
import movieSearchQuery from './queries/movieSearchQuery.graphql';
import './styles.scss';

const years = range(0, 32, 1);

const Home = () => {
  const history = useHistory();
  const { search: searchQuery } = useLocation();
  const { s, p = '1', y } = parseQuery(searchQuery);
  const [ search, setSearch ] = useState(s || '');
  const [ page, setPage ] = useState(p || '');
  const [ year, setYear ] = useState(y || '');
  const [ payload, setPayload ] = useState({
    search,
    page: parseInt(page, 10) || 1,
    year: y || ''
  });
  const [ total, setTotal ] = useState(0);
  const [ response, setResponse ] = useState(false);
  const [ searchResults, setSearchResult ] = useState([]);
  const { loading } = useQuery(movieSearchQuery, {
    skip: payload.search === '',
    variables: payload,
    onCompleted: (data) => {
      const { movieSearch } = data || {};
      const { Search, totalResults, Response } = movieSearch || {};

      setSearchResult(Search);
      setTotal(totalResults);
      setResponse(Response === 'True');
    }
  });

  useEffect(() => {
    history.listen((location) => {
      const { search: searchQuery } = location;
      const { s, p = '1', y } = parseQuery(searchQuery);

      setSearch(s);
      setPage(p);
      setYear(y);

      setPayload({
        search: s,
        page: parseInt(p, 10) || 1,
        year: y || ''
      });
    });

    return () => {
      history.listen(() => {});
    }
  });

  const clearResults = () => {
    setSearchResult([]);
    setTotal(0);
  }

  const handleFormSubmit = useCallback((e) => {
    e.preventDefault();

    clearResults();

    history.push(`/?${stringifyQuery({
      s: search,
      p: 1,
      y: year
    })}`);

  }, [search, year]);

  return (
    <div className="home">
      <div className="hero">
        <div className="text">
          <h2>Welcome</h2>
          <p>Millions of movies, TV shows to discover. Explore now.</p>
        </div>
        <form onSubmit={handleFormSubmit} className="search-form d-flex">
          <div className="input-group">
            <input value={search} placeholder="Search for a movie, tv show..." onChange={e => setSearch(e.target.value)} />
            <select value={year} onChange={e => setYear(e.target.value)}>
              <option value="">any</option>
              {years.map(y => <option key={`year-${y}`}>{1990+y}</option>)}
            </select>
            <Button type="submit">Search</Button>
          </div>
        </form>
      </div>
      <div className="container-fluid">
        {loading ? <Loading /> : (
          <p>{response ? `Showing ${(searchResults || []).length} of ${total} results` : 'Type and search for a movie...'}</p>
        )}
        <div id="result">
          {(searchResults || []).map((res) => (
            <ResultItem
              key={`item-${res.imdbID}`}
              data={res}/>
          ))}
        </div>
        <div className="text-center">
          {total > 10 && (
            <Pagination
              lastPage={Math.floor(total / 10)}
              onSet={() => {
                clearResults();

                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
              }} />
          )}
        </div>
      </div>
    </div>
  );
};

export default Home;
