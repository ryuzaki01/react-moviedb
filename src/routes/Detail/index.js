import React, {useEffect, useState, useCallback} from 'react';
import {useQuery} from '@apollo/react-hooks';
import {useParams} from 'react-router-dom';
import { Icon } from '@tiket-com/react-ui';

import Loading from '../../components/Loading';

import movieDetailQuery from './queries/movieDetailQuery.graphql';
import NoImage from "../../assets/no-image.png";
import './styles.scss';

const Detail = () => {
  const {id} = useParams();
  const [liked, setLiked] = useState(false);
  const {loading, data} = useQuery(movieDetailQuery, {
    skip: id === '',
    variables: {
      id
    }
  });
  const {movieDetail} = data || {};
  const {
    Title,
    Year,
    Rated,
    Genre,
    Released,
    Director,
    Writer,
    Actors,
    Country,
    Language,
    Runtime,
    Poster,
    Plot,
  } = movieDetail || {};

  useEffect(() => {
    if (!id) {
      return;
    }

    const getFavorited = async () => {
      const value = await db.get(id);

      setLiked(!!value);
    }

    getFavorited();
  }, [id]);

  const handleLike = useCallback(() => {
    if (liked) {
      db.remove(id);
      setLiked(false);
    } else {
      db.add({
        id,
        ...movieDetail
      });
      setLiked(true);
    }
  }, [liked, movieDetail]);

  if (loading) {
    return <div className=""><Loading/></div>
  }

  return (
    <div className="container-fluid detail">
      {loading ? (
        <Loading full/>
      ) : (
        <>
          <div className="poster">
            <img src={Poster === 'N/A' ? NoImage : Poster}/>
            <Icon icon={liked ? 'star-full' : 'star-blank'} className="favorite" onClick={handleLike}/>
          </div>
          <div className="content">
            <div className="title">
              <b>{Title}</b>
              <span>{`(${Year})`}</span>
            </div>
            <div className="subtitle">
              <span className="rate">{Rated}</span>
              <span className="release">{Released}</span>
              <span className="genre">{Genre}</span>
              <span className="runtime">{Runtime}</span>
            </div>
            <div className="overview">
              <h3>Overview</h3>
              <p>{Plot}</p>
            </div>
            <div className="extras">
              <div>
                <label>Director</label>
                <span>{Director}</span>
              </div>
              <div>
                <label>Writer</label>
                <span>{Writer}</span>
              </div>
              <div>
                <label>Actors</label>
                <span>{Actors}</span>
              </div>
              <div>
                <label>Country</label>
                <span>{Country}</span>
              </div>
              <div>
                <label>Country</label>
                <span>{Country}</span>
              </div>
              <div>
                <label>Language</label>
                <span>{Language}</span>
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Detail;
