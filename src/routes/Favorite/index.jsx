import React, { useEffect, useState } from 'react';

import Index from "../../components/ResultItem";
import Pagination from '../../components/Pagination';
import Loading from '../../components/Loading';

import './styles.scss';

const Favorite = () => {
  const [ loading, setLoading ] = useState(true);
  const [ total, setTotal ] = useState(0);
  const [ favorites, setFavorites ] = useState([]);

  const clearResults = () => {
    setFavorites([]);
    setTotal(0);
  }

  useEffect(() => {
    const loadFavoriteData = async () => {
      const results = await db.load();

      setFavorites(results);
      setLoading(false);
      setTotal((results || []).length);
    }

    loadFavoriteData();
  }, [])

  return (
    <div className="container-fluid favorite">
      <h2>Your Movie List</h2>
      {loading ? <Loading /> : (
        <p>{total > 0 ? `Showing ${(favorites || []).length} of ${total} results` : 'You have no movie list, go search and add some'}</p>
      )}
      <div id="result">
        {(favorites || []).map((res) => (
          <Index
            key={`item-${res.imdbID}`}
            data={res}/>
        ))}
      </div>
    </div>
  );
};

export default Favorite;
